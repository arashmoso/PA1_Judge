from ..base.test import Test, grade
import copy
import socket, struct
import time

class DataSend(Test):
    description = "Data Sending Test"
    order = 2
    enabled = True
    test_order = ['test_send_expo', 'test_send_linear', 'test_send_timeout', 'test_send_errors']
    save_judge_mode = False

    def before(self):
        client_count = 3
        self.kill_clients()
        self.new_map()
        client_dict = {0:'ce', 1:'cl', 2:'ce'}

        self.start_clients(client_dict=client_dict)

        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)
    # ===================================================================================================================================================== #

    @grade(30)
    def test_send_expo(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('send 8000 1.1.1.17:9000 "abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz"')
        time.sleep(self.sleep_time)
        for i in range(7):
            self.clients[1].write_io("forward 1")
            time.sleep(self.sleep_time)
            self.clients[1].write_io("forward 0")
            time.sleep(self.sleep_time)

        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_send_expo')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=6)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=8)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=6)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=5)
            self.assert_true(self.check_send_frames(2), message='send frames node 2 did not match', end=False, grade=5)


    @grade(35)
    def test_send_linear(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('send 9000 100.100.100.101:8000 "abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz"')
        time.sleep(self.sleep_time)
        for i in range(5):
            self.clients[1].write_io("forward 1")
            time.sleep(self.sleep_time)
        self.clients[1].write_io("drop 1")
        time.sleep(self.sleep_time)
        for i in range(11):
            self.clients[1].write_io("forward 0")
            time.sleep(self.sleep_time)
        self.clients[1].write_io("drop 0")
        time.sleep(self.sleep_time)
        for i in range(14):
            self.clients[1].write_io("forward 0")
            time.sleep(self.sleep_time)

        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_send_linear')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=8)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=8)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=8)
            self.assert_true(self.check_send_frames(2), message='send frames node 2 did not match', end=False, grade=5)
            
            self.assert_true(self.check_recv_frames(2), message='recv frames node 2 did not match', end=False, grade=6)

    @grade(25)
    def test_send_timeout(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io(
            'send 8000 1.1.1.17:9000 "abcdefghijklmnopqrstuvwxyzabcdefghijkl"')
        time.sleep(self.sleep_time)
        for i in range(6):
            self.clients[1].write_io("forward 0")
            time.sleep(self.sleep_time)
        self.clients[1].write_io("forward 12")
        time.sleep(self.sleep_time)
        for i in range(10):
            self.clients[1].write_io("forward 0")
            time.sleep(self.sleep_time)

        time.sleep(self.sleep_time)
        self.client_manager.get_clients_ready_to_judge('log/test_send_timeout')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=6)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=4)
            self.assert_true(self.check_output(1), message='Output for node 1 did not match', end=False, grade=6)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=5)
            
            self.assert_true(self.check_send_frames(2), message='send frames node 2 did not match', end=False, grade=4)

    @grade(10)
    def test_send_errors(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io(
            'send 8000 1.1.31.17:9000 "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"')
        time.sleep(self.sleep_time)
        self.clients[0].write_io(
            'send 8000 1.1.1.17:9 "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"')
        time.sleep(self.sleep_time)
        self.clients[0].write_io(
            'send 8000 1.1.1.17:9000 "abcdefghijklmnopqrstuvwxyzp"')
        time.sleep(self.sleep_time)
        self.clients[1].write_io("forward 2")
        time.sleep(self.sleep_time)
        self.clients[0].write_io(
            'send 8000 1.1.1.17:9000 "test"')
        time.sleep(self.sleep_time)
        for i in range(3):
            self.clients[1].write_io("forward 0")
            time.sleep(self.sleep_time)

        time.sleep(self.sleep_time)
        self.client_manager.get_clients_ready_to_judge('log/test_send_errors')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=6)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False,
                             grade=2)
            self.assert_true(self.check_send_frames(2), message='send frames node 2 did not match', end=False,
                             grade=2)

# ===================================================================================================================================================== #
