from ..base.test import Test, grade
import copy
import socket, struct
import time

class ConnectPhase(Test):
    description = "Connecting Test Only"
    order = 1
    enabled = True
    test_order = ['test_connect_simple', 'test_connect_errors', 'test_connect_multiple']
    save_judge_mode = False

    def before(self):
        client_count = 3
        self.kill_clients()
        self.new_map()
        client_dict = {0: 'ce', 1: 'cl', 2: 'ce'}

        self.start_clients(client_dict=client_dict)

        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)

    # ===================================================================================================================================================== #

    @grade(50)
    def test_connect_simple(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_connect_simple')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=10)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=10)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=15)
            self.assert_true(self.check_recv_frames(0), message='receive frames node 0 did not match', end=False, grade=15)

    @grade(30)
    def test_connect_errors(self):
        self.clients[0].write_io('connect 8000 1.1.3.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('connect 9000 100.100.100.101:8000')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_connect_errors')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=10)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=10)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=10)
            self.assert_true(self.check_recv_frames(0), message='receive frames node 0 did not match', end=False,grade=0)

# ===================================================================================================================================================== #

    @grade(20)
    def test_connect_multiple(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('connect 9000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[2].write_io('connect 8000 100.100.100.101:8000')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_connect_multiple')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=10)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=10)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=0)
            self.assert_true(self.check_recv_frames(0), message='receive frames node 0 did not match', end=False, grade=0)
