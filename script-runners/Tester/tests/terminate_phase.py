from ..base.test import Test, grade
import copy
import socket, struct
import time

class DisconnectPhase(Test):
    description = "Disconnecting Test"
    order = 3
    enabled = True
    test_order = ['test_disconnect_simple', 'test_disconnect_errors']
    save_judge_mode = False

    def before(self):
        client_count = 3
        self.kill_clients()
        self.new_map()
        client_dict = {0: 'ce', 1: 'cl', 2: 'ce'}

        self.start_clients(client_dict=client_dict)

        for client in self.clients.itervalues():
            client.wait_for_start()
        time.sleep(self.sleep_time)

    def after(self):
        self.kill_clients()
        self.free_map()

    def save_judge(self, path):
        self.client_manager.save_judge_all(path)

    def load_judge(self, path):
        self.client_manager.load_judge_all(path)

    # ===================================================================================================================================================== #

    @grade(40)
    def test_disconnect_simple(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('close 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_disconnect_simple')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=10)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=10)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=10)
            self.assert_true(self.check_recv_frames(0), message='receive frames node 0 did not match', end=False, grade=10)

    @grade(60)
    def test_disconnect_errors(self):
        self.clients[0].write_io('connect 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('close 8000 1.1.13.17:9000')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('close 8000 1.1.1.17:90')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('send 8000 1.1.1.17:9000 "hello world"')
        time.sleep(self.sleep_time)
        self.clients[1].write_io('forward 1')
        time.sleep(self.sleep_time)
        self.clients[0].write_io('close 8000 1.1.1.17:9000')
        time.sleep(self.sleep_time)

        self.client_manager.get_clients_ready_to_judge('log/test_disconnect_errors')
        if not self.save_judge_mode:
            self.assert_true(self.check_output(0), message='Output for node 0 did not match', end=False, grade=15)
            self.assert_true(self.check_output(2), message='Output for node 2 did not match', end=False, grade=15)
            self.assert_true(self.check_send_frames(0), message='send frames node 0 did not match', end=False, grade=15)
            self.assert_true(self.check_recv_frames(0), message='receive frames node 0 did not match', end=False,grade=15)
